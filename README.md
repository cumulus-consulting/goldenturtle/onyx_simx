# Onyx Simx

The Onyx Simx is an always-on simulation that is shared across all users. It has a fixed topology defined here:

<img src="https://gitlab.com/cumulus-consulting/goldenturtle/onyx_simx/-/raw/master/onyx_topology.png" title="Network Topology" />

Since this topology is shared, all users must ensure that any changes have been socialized with other active users. The best places to communicate intentions are in the `#swnvex-net-cumulus-gts` channel in the `nvidia.slack.com` group.

The username/password to log in via the console has been disabled.

To gain access to the simulation, contact the CITC team and your SSH key can be added to the simulation.


## Simulation Fundamentals

The Onyx simulation is built on top of Simx so there is hardware emulation capability under the hood.

## Logging into Onyx

To log into the Onyx switches, use the following credentials:

| Credential | Value |
|----------|:--------|
| Username | admin |
| Password | admin |

